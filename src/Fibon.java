import java.io.*;
import java.math.BigInteger;

import parcs.*;

public class Fibon implements AM {

    static int daemonsCount = 4;
    static long rangeTo = 10000000;

    public static void main(String[] args) {
        System.out.println("--- Start ---");
        try {
            daemonsCount = Integer.parseInt(args[0], 10);
        } catch (Exception e) {
            System.out.println("--- Invalid daemonsCount param ---");
        }
        try {
            rangeTo = Long.parseLong(args[1], 10);
        } catch (Exception e) {
            System.out.println("--- Invalid rangeTo param ---");
        }
        task curtask = new task();
        curtask.addJarFile("Fibon.jar");
        (new Fibon()).run(new AMInfo(curtask, (channel)null));
        curtask.end();
        System.out.println("--- End ---");
    }

    public void run(AMInfo info) {
      channel[] channels = new channel[daemonsCount];

      long n = rangeTo / daemonsCount;

      for (int i = 0; i < daemonsCount; ++i) {
          point p = info.createPoint();
          channel c = p.createChannel();
          p.execute("Fib");
          c.write(BigInteger.valueOf(n * i + 1).toString() + " " + BigInteger.valueOf(n * (i + 1) + 1));
          channels[i] = c;
      }

      long start = System.currentTimeMillis();
      int res = 0;
      for (int i = 0; i < channels.length; ++i) {
          long r = channels[i].readLong();
          System.out.println("Received response " + r + " from channel " + i);
          res += r;
      }
      System.out.println(res);
      System.out.println("TIME " + (System.currentTimeMillis() - start));
    }
}
