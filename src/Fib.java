import parcs.*;

import java.math.BigInteger;

public class Fib implements AM{
    public void run(AMInfo info){
        String message = info.parent.readObject().toString();
        System.out.println("-- READ FROM PARENT --- " + message);

        String[] ranges = message.split(" ");

        long from = new BigInteger(ranges[0]).longValue();
        long to = new BigInteger(ranges[1]).longValue();

        long res = 0;
        for (long i = from; i <= to; ++i) {
            if (BigInteger.valueOf(i).isProbablePrime(10)) {
                res++;
                System.out.println("Prime found: " + i);
            }
        }

        info.parent.write(res);
    }
}
